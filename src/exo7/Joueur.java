package exo7;

class Joueur implements Comparable<Joueur> {

	
	private String name;
	private int age;
	
	public Joueur() {	
		
	}
	
	

	public Joueur(String name, int age) {
		this.name = name;
		this.age = age;
	}



	@Override
	public String toString() {
		return "Joueur: nom =" + name + ",age= " + age + "\n" ;
	}
     

	public String getName() {
		return this.name;
	}
	public int getAge() {
		return this.age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	

		

		public int compareTo(Joueur j) {
			return name.compareTo(j.name);
		}

	
}



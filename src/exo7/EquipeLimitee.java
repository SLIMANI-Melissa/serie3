package exo7;


import java.util.ArrayList;
import java.util.Collection;



public class EquipeLimitee {
	
		Collection<Joueur> equipe = new ArrayList<>();

		private int nombreMax;
		
		public EquipeLimitee() {
		
		} 
		
		
		public EquipeLimitee(int nombreMax) {
			this.nombreMax = nombreMax;
			
		}

		public void addlimite(Joueur e ) {
		
				if( equipe.size()<this.nombreMax )
				{
					 equipe.add(e);
					
				}
				else 
					System.out.println("l'equipe est complete ");
				 
			 }
			
		

		public boolean remove(Joueur o) {
			return equipe.remove(o);
		}

		public boolean contains(Joueur o) {
			return equipe.contains(o);
		}


		
		public String toString() {
			System.out.println("Equipe : " + equipe.size() + "joueurs\n");
			for(Joueur element : equipe) {
			System.out.println( element);
			}
			return null;
		}
		
		

		public void addAllEquipe(Equipe e2) { 
			this.equipe.removeAll(e2.equipe);
			this.equipe.addAll(e2.equipe);
		}
		
		 public int getNombreJoueurs(){
			 return this.equipe.size();
		}
		
		  public double getMoyenneAge() {
			  int somme=0; 
			  for(Joueur element : equipe) {
				  somme=element.getAge()+somme;
			  }
			  
			  return somme/this.equipe.size(); 
		  }
		

		public void clear() {
			equipe.clear();
		} 
		
		
		
}

	

